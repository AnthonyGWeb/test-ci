<?php

namespace App;

class Db
{
    const DATABASE_DSN = "mysql:host=db;dbname=web;";
    const DATABASE_USERNAME = "db_user";
    const DATABASE_PASSWORD = "db_pass";

    /** @var \PDO */
    private $pdo;

    public function __construct()
    {
        $this->connect();
        $this->pdo->query('CREATE TABLE IF NOT EXISTS test (id INT AUTO_INCREMENT PRIMARY KEY, total INT DEFAULT 0);')->execute();
        $this->pdo->query('INSERT INTO test (total) VALUES (0)');
    }

    public function connect()
    {
        $this->pdo = new \PDO(self::DATABASE_DSN, self::DATABASE_USERNAME, self::DATABASE_PASSWORD);
    }

    /**
     * @param string $statement
     * @return array
     */
    public function query(string $statement)
    {
        return $this->pdo->query($statement)->fetchAll();
    }

    public function getNames()
    {
        return ['Benjamin', 'Raphael', 'Anthony'];
    }
}
